# eks-playground


This README provides instructions on how to deploy a simple Amazon EKS (Elastic Kubernetes Service) cluster using GitLab that runs a php app, postgres db & redis.

## Steps

### 1. Copy project content to your own gitlab repo

### 2. Connect to EKS Cluster on GitLab

Follow these steps on GitLab:
- Navigate to 'Operations' > 'Kubernetes Clusters'.
- Click on 'Connect cluster'.
- Register 'eks-agent' and store the secret token, as you will need it later.
### 3. Set EKS Agent in `.gitlab-ci.yml`

Modify the `.gitlab-ci.yml` file in your repository to set the EKS_AGENT variable.

```yaml
variables:
  EKS_AGENT: "MahmoudAlyy/eks-playground:eks-agent"
```

Replace `MahmoudAlyy/eks-playground` with your own group/namespace and project name

### 4. Configure CI/CD Variables in GitLab

Go to your project's 'Settings' > 'CI / CD' and expand the 'Variables' section. Add the following variables:  
(You can use root account cred for simplicity but just know that this is not the best practice here.)

- `AWS_ACCESS_KEY_ID`: Your AWS access key ID.
- `AWS_SECRET_ACCESS_KEY`: Your AWS secret access key.
- `TF_VAR_agent_token`: The agent token you obtained earlier.
- `TF_VAR_kas_address`: Set this to `wss://kas.gitlab.com`.

### 5. Deploy the Cluster

With the configuration complete, your GitLab pipeline should be ready to deploy the EKS cluster. Commit any changes and push them to your repository to start the deployment process.

## Accessing the Web Application

Once the EKS cluster is deployed, you can access the web application as follows:

1. **Update your Kubernetes Configuration:**

   Use the AWS CLI to update your `kubeconfig` file:

   ```bash
   aws eks --region us-east-2 update-kubeconfig --name gitlab-terraform-eks
   ```

2. **Port Forward the Web Application Service:**

   Run the following command to forward the local port 8080 to the web application service running in the cluster:

   ```bash
   kubectl port-forward service/php-webapp-service 8080:80
   ```

3. **Access the Web Application:**

   Open your web browser and navigate to `http://127.0.0.1:8080`.

## Cleanup
After completing your testing and evaluations with the EKS cluster, it's essential to properly clean up the resources to avoid unnecessary costs. Follow these steps to ensure a safe and complete cleanup:

- Navigate to the pipeline page in your GitLab project.
- Locate and execute the 'Cleanup' job. This job is configured to run the terraform destroy command, which will de-provision all the resources that were created.
