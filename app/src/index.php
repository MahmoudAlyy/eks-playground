<?php

// Read database configuration from environment variables
$databaseHost = getenv('DB_HOST');
echo "Database Host: " . $databaseHost . "<br>";

$databasePort = '5432';
echo "Database Port: " . $databasePort . "<br>";

$databaseName = 'postgres';
echo "Database Name: " . $databaseName . "<br>";

$databaseUsername = getenv('DB_USERNAME');
echo "Database Username: " . $databaseUsername . "<br>";

$databasePassword = getenv('DB_PASSWORD');
echo "Database Password: " . $databasePassword . "<br>";

// Read Redis configuration from environment variables
$redisHost = getenv('REDIS_HOST');
echo "Redis Host : " . $redisHost . "<br>";

$redisPort = '6379';

echo "Hello, World from Docker! <br>";

try {
    // Connect to PostgreSQL database
    $pdo = new PDO("pgsql:host=$databaseHost;port=$databasePort;dbname=$databaseName;user=$databaseUsername;password=$databasePassword");

    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    echo "Connected to PostgreSQL database successfully<br>";

    // Connect to Redis cache
    $redis = new Redis();
    $redis->connect($redisHost, $redisPort);
    echo "Connected to Redis cache successfully<br>";

    // Example PostgreSQL query
    $stmt = $pdo->prepare("SELECT 'DB test success' AS test");
    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);    
    echo "DB test: " . $result[0]['test'] . "<br>";
    
    // Example Redis operation
    $redis->set('example_key', 'Hello, Redis!');
    $value = $redis->get('example_key');
    echo "Redis value: " . $value . "<br>";

} catch (PDOException $e) {
    echo "Database connection failed: " . $e->getMessage();
} catch (RedisException $e) {
    echo "Redis connection failed: " . $e->getMessage();
}

?>